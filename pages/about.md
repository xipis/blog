---
layout: page
title: 关于
---





### Hi there 👋

<a href="https://www.guofei.site/2019/08/31/github_star_counter.html"><img align="right" alt="guofei9987's github stats" width="40%" src="https://github-profile-trophy.vercel.app/?username=guofei9987&title=Stars,Followers&row=1&column=2"></a>


- 👯 目前在蚂蚁集团/天堑实验室/算法专家。曾任京东集团/Y事业部/算法专家。
- 🧮 热爱算法。[2021年，人类首次在另一个星球（火星）实现动力飞行](https://github.com/guofei9987?achievement=mars-2020-contributor&tab=achievements)，本人因开源贡献获得 NASA 徽章
- 🔭 技术积累：[技术博客](https://www.guofei.site/) 累积 <img alt="Blog Counts" src="https://www.guofei.site/pages/blog_cnt.svg"> 篇
- 📖 喜欢读书：[读书笔记](https://www.guofei.site/reading/#/) <img alt="Blog Counts" src="https://www.guofei.site/reading/media/reading_words.svg">，读书 <img alt="Blog Counts" src="https://www.guofei.site/reading/media/book_cnt.svg">
- 🤔 one of 121 [authors](https://github.com/scipy/scipy/issues/7798) in [scipy 1.0.0](https://github.com/scipy/scipy/releases/tag/v1.0.0)
- 📚 学术期刊 [Journal of Finance and Accounting](http://www.sciencepublishinggroup.com/journal/index?journalid=171) **[审稿员](https://www.guofei.site/pages/certification.html#Reviewer)**
- 🏃 爱好运动，能做 30 个 <b><a href="https://www.bilibili.com/video/BV1L64y1t7Ef/" target="_blank">单手俯卧撑</a></b>
- 📫 <a href="https://www.zhihu.com/people/guo-fei-16-12/answers/by_votes" target="_blank"><img alt="Blog Counts" src="https://www.guofei.site/guofei9987/zhihu.svg"></a>


<!-- - 🌱 历史工作经历：京东集团Y事业部（动态定价算法） -->

-------------------

[![scikit-opt](https://github-readme-stats.vercel.app/api/pin/?username=guofei9987&repo=scikit-opt&theme=radical)](https://github.com/guofei9987/scikit-opt)
[![blind_watermark](https://github-readme-stats.vercel.app/api/pin/?username=guofei9987&repo=blind_watermark&theme=radical)](https://github.com/guofei9987/blind_watermark)
[![text_blind_watermark](https://github-readme-stats.vercel.app/api/pin/?username=guofei9987&repo=text_blind_watermark&theme=radical)](https://github.com/guofei9987/text_blind_watermark)
[![guofei9987.github.io](https://github-readme-stats.vercel.app/api/pin/?username=guofei9987&repo=guofei9987.github.io&theme=radical)](https://github.com/guofei9987/guofei9987.github.io)
[![fourier_artist](https://github-readme-stats.vercel.app/api/pin/?username=guofei9987&repo=fourier_artist&theme=radical)](https://github.com/guofei9987/fourier_artist)
[![pyLSHash](https://github-readme-stats.vercel.app/api/pin/?username=guofei9987&repo=pyLSHash&theme=radical)](https://github.com/guofei9987/pyLSHash)
[![github_star_counter](https://github-readme-stats.vercel.app/api/pin/?username=guofei9987&repo=github_star_counter&theme=radical)](https://github.com/guofei9987/github_star_counter)
[![plot2svg](https://github-readme-stats.vercel.app/api/pin/?username=guofei9987&repo=plot2svg&theme=radical)](https://github.com/guofei9987/plot2svg)



-------------------

I contributed:

[![scipy](https://github-readme-stats.vercel.app/api/pin/?username=scipy&repo=scipy&theme=radical)](https://github.com/scipy/scipy)
[![statsmodels](https://github-readme-stats.vercel.app/api/pin/?username=statsmodels&repo=statsmodels&theme=radical)](https://github.com/guofei9987/statsmodels)

---------------------



如何找到我：  

[<i class="fa fa-github fa-lg" style="color:#16a095;font-size:70px;"></i>](https://github.com/guofei9987/)

![](http://www.guofei.site/public/donate/qr_wechat.jpg)
